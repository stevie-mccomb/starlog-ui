(function () {
    var toTop = document.getElementById('back-to-top');

    function scrollToTop()
    {
        var intval = setInterval(function () {
            var pos = window.scrollY;
            var top = 0;

            if (pos <= top) {
                clearInterval(intval);
            }

            window.scrollTo(0, window.scrollY - 20);
        }, 10);
    };

    toTop.addEventListener('click', scrollToTop);
})();
