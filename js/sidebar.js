/**
 * Controls the opening and closing of the sidebar/container.
 */
(function () {
    /**
     * A reference to the container element.
     *
     * @var Element
     */
    var container = document.getElementById('container');

    /**
     * A reference to the sidebar element.
     *
     * @var Element
     */
    var sidebar = document.getElementById('primary-sidebar');

    /**
     * Opens the sidebar.
     *
     * @return void
     */
    function open()
    {
        container.className = 'container open';
    };

    /**
     * Closes the sidebar.
     *
     * @return void
     */
    function close()
    {
        container.className = 'container';
    };

    /**
     * DOM event listeners
     */
    sidebar.addEventListener('mouseenter', open);
    sidebar.addEventListener('mouseleave', close);
})();
